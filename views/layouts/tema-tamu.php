<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-04-22 16:45:16
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-26 21:03:08
*/

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

use kartik\icons\Icon;
Icon::map($this);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <style>
    /* Note: Try to remove the following lines to see the effect of CSS positioning */
    .affix {
      top: 0;
      width: 100%;
      z-index: 9999 !important;
    }

    .affix + .container-fluid {
      padding-top: 83px;
    }
    </style>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <link href="https://fonts.googleapis.com/css?family=Marck+Script|Signika+Negative" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <?php $this->head() ?>
</head>
<body style="background-color: #fdfdfd">
<?php $this->beginBody() ?>

<div class="wrap">
    
    <div class="container-fluid" style="background-color:#085886;color:#fff;height:108px;">
        <div class="col-md-offset-1 col-md-11" style="padding:10px">
            <div class="row">
                  <img style="float:left; padding-right:0px" width="60px" src="<?=Yii::getAlias('@web')?>/logo/riau.png"/>
                  <div class="content-heading" style="">
                    <h3 style="margin-bottom: 0px; padding-left:75px; color:white; font-family: 'Marck Script', cursive;"><strong>Rumah Sakit Umum Daerah Petala Bumi</strong></h3>
                      <span style="padding-left:15px; font-family: 'Poppins', sans-serif; font-size:18px;"><strong>Sistem Informasi Rujukan PONEK Terintegrasi</strong></span>
                  </div>
            </div>
        </div>
    </div>


    <?php
    NavBar::begin([
        // 'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            // 'class' => 'navbar-inverse navbar-fixed-top',
            'class' => 'navbar navbar-default affix untuk-font container-fluid',
            'data-spy' => 'affix',
            'data-offset-top' => '108',
            'style' => 'border-radius:0;'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav navbar-left container-fluid'],
        'items' => [
            ['label' => Icon::show('home'). ' Beranda', 'url' => ['/site/index']],
        ],
        'encodeLabels' => false
    ]);
    NavBar::end();
    ?>

    <!-- <div class="container untuk-font"> -->
    <div class="container-fluid untuk-font">
        <div style="padding: 0% 4% 34% 4%">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 untuk-font" >
                <h4 class="title untuk-font">Alamat</h4>
                <p>
                    <strong>Jalan Dr. Soetomo No. 65, Sekip, LimaPuluh,<br>Kota Pekanbaru, Riau 28155</strong>
                </p>
                <p>
                    <?= Icon::show('clock-o')?> &nbsp;&nbsp;Buka 24 Jam<br>
                    <?= Icon::show('phone')?> &nbsp;&nbsp;(0761) 23024 &nbsp;Ext. 118
                </p>
                <p></p>
            </div>
            <div class="col-sm-6">
                <h4 class="title untuk-font">Rumah Sakit Umum Daerah Petala Bumi</h4>
                <h4 class="title untuk-font">Provinsi Riau</h4>
            </div>
            </div>
        <hr>
        <div class="row text-center untuk-font"><a href="http://rsudpetalabumi.riau.go.id/" target="blank" style="color: #fff;">Copyright © RSUD Petala Bumi 2018</a></div>
    </div>  
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
