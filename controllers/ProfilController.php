<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-04 15:08:12
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-21 15:57:06
*/

namespace app\controllers;

use Yii;
use app\models\Pengguna;
use app\models\UbahPassword;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use app\components\AccessRule;
use app\models\User;

class ProfilController extends Controller
{

	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => [
                    'edit', 'ubah-profil', 'ubah-password'
                ],
                'rules' => [
                    [
                        'actions' => ['edit', 'ubah-profil', 'ubah-password'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_SUPERADMIN,
                            User::ROLE_ADMIN,
                            User::ROLE_USER,
                        ],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'ubah-profil' => ['POST'],
                    'ubah-password' => ['POST'],
                ],
            ],
        ];
    }

	public function actionEdit()
	{
		$model = Pengguna::findOne(Yii::$app->user->identity->id);

		$passmodel = new UbahPassword();
		$passmodel->id = $model->id;
		$passmodel->username = $model->username;

		return $this->render('edit',[
			'model' => $model,
			'passmodel' => $passmodel
		]);
	}

	public function actionUbahProfil($id)
	{
		$model = Pengguna::findOne($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Profil Berhasil diubah.');
            return $this->redirect(['profil/edit']);
        }
	}

	public function actionUbahPassword()
	{
		$passmodel = new UbahPassword();
		$passmodel->load(Yii::$app->request->post());

		if($passmodel->pass_baru!=$passmodel->pass_baru2){
			Yii::$app->getSession()->setFlash('error', 'Password baru anda tidak sama, harap lebih teliti.');
			return $this->redirect(['profil/edit']);
		}else{
			$model = Pengguna::findOne($passmodel->id);
			if($passmodel->pass_lama!=$model->password){
				Yii::$app->getSession()->setFlash('error', 'Password lama yang anda masukkan tidak sesuai dengan Password anda saat ini.');
				return $this->redirect(['profil/edit']);
			}else{
				$model->password = $passmodel->pass_baru;
				$model->save();
				Yii::$app->getSession()->setFlash('success', 'Password anda berhasil diubah.');
				return $this->redirect(['profil/edit']);
			}
		}
	}
}